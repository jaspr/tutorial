<?php

/**
 * Created by tomas
 * at 06.08.2023
 */

declare(strict_types=1);

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Request\Parser;
use Slim\Psr7\Factory\ServerRequestFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

require_once __DIR__ . '/../vendor/autoload.php';

/*
 * In this example we will show how to create ParserRequest from Request\Parser
 */
$cache = new Psr16Cache(new ArrayAdapter());
$driver = new AnnotationDriver();
$metadata = MetadataFactory::create(
    [__DIR__ . '/../src/Entity'],
    $cache,
    $driver
);

/*
 * Base URL is root where your API lays
 */
$baseUrl = 'http://localhost';
/*
 * This is minimal requirement for URI Parser to create
 */
$uri = new Parser($baseUrl, $metadata);

/*
 * Then we need request to parse. This SDK is PSR compatible, so we work with PSR ServerRequestInterface. Here for
 * example we use Slim PSR implementation.
 */
$rf = new ServerRequestFactory();
$url = $baseUrl;
$url .= '/foos'; // This is our collection endpoint
$url .= '/uuid'; // This is resource ID
$url .= '?filter=prop eq \'data\''; // This is one of filter format this SDK poses
$url .= '&fields[foos]=prop'; // This reduce returned fields
$url .= '&sort=-prop'; // This is sort in recommended format
$url .= '&include=relation'; // This can include additional data to response
$url .= '&page[limit]=50&page[offset]=100'; // This is one of pagination strategy, in this case limit-offset strategy

$request = $rf->createServerRequest(RequestMethodInterface::METHOD_GET, $url);

/*
 * There we go. $parsed now contains parsed request
 */
$parsed = $uri->parse($request);

echo "We have information about PATH\n";
echo "==============================\n";
print_r($parsed->getPath());

echo "We have information about FILTER\n";
echo "================================\n";
print_r($parsed->getFilter());

echo "We have information about SPARSE FIELDS\n";
echo "=======================================\n";
print_r($parsed->getFieldset());

echo "We have information about SPARSE FIELDS\n";
echo "=======================================\n";
print_r($parsed->getSort());

echo "We have information about INCLUSION\n";
echo "===================================\n";
print_r($parsed->getInclusion());

echo "We have information about PAGINATION\n";
echo "====================================\n";
print_r($parsed->getPagination());

/*
 * In case of POST or PATCH we can retrieve body data like this. The return value is instance of Document.
 */
$document = $parsed->getBody();
