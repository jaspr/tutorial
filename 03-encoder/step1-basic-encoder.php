<?php

/**
 * Created by tomas
 * at 05.08.2023
 */

declare(strict_types=1);

use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Tutorial\Entity\Foo;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

require_once __DIR__ . '/../vendor/autoload.php';

/*
 * We gonna need metadata for encoder construct
 */
$cache = new Psr16Cache(new ArrayAdapter());
$driver = new AnnotationDriver();
$metadata = MetadataFactory::create(
    [__DIR__ . '/../src/Entity'],
    $cache,
    $driver
);

/*
 * Then we create encoder instance
 */
$encoder = new Encoder($metadata);

/*
 * Create example data
 */
$entity = new Foo();

/*
 * You can compare next two outputs
 */
$identifier = $encoder->identify($entity);
echo "Identifier" . PHP_EOL;
echo "========" . PHP_EOL;
echo json_encode($identifier, JSON_PRETTY_PRINT) . PHP_EOL . PHP_EOL;

$resource = $encoder->encode($entity);
echo "Resource" . PHP_EOL;
echo "========" . PHP_EOL;
echo json_encode($resource, JSON_PRETTY_PRINT) . PHP_EOL . PHP_EOL;

/*
 * As you can see, results are same. This is because of lack of processors. In next step we will use them.
 */
