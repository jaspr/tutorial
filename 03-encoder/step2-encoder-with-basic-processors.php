<?php

/**
 * Created by tomas
 * at 05.08.2023
 */

declare(strict_types=1);

use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\EncoderFactory;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Tutorial\Entity\Foo;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

require_once __DIR__ . '/../vendor/autoload.php';

$cache = new Psr16Cache(new ArrayAdapter());
$driver = new AnnotationDriver();
$metadata = MetadataFactory::create(
    [__DIR__ . '/../src/Entity'],
    $cache,
    $driver
);

$entity = new Foo();

/*
 * This time we use EncoderFactory for instancing encoder.
 * This encoder will have basic processors for Attribute, Relationship and Meta
 */
$encoder = EncoderFactory::createDefaultEncoder($metadata);

/*
 * Encoder can provide identifier
 */
$identifier = $encoder->identify($entity);
echo "Identifier" . PHP_EOL;
echo "========" . PHP_EOL;
echo json_encode($identifier, JSON_PRETTY_PRINT) . PHP_EOL . PHP_EOL;

/*
 * Or whole resource
 */
$resource = $encoder->encode($entity);
echo "Resource" . PHP_EOL;
echo "========" . PHP_EOL;
echo json_encode($resource, JSON_PRETTY_PRINT) . PHP_EOL . PHP_EOL;

/*
 * Or whole document
 */
$document = $encoder->compose($entity);
echo "Document" . PHP_EOL;
echo "========" . PHP_EOL;
echo json_encode($document, JSON_PRETTY_PRINT);

/*
 * This means encoder can work independently, so you can use it for other cases, when you want to format your data to
 * rich JSON objects.
 */
