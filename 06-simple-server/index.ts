import type {Resource, ToOneRelationship, ToManyRelationship, Index} from "@jaspr/client-js";

export interface Bar extends Resource {
}

export interface Foo extends Resource {
    attributes: {
        prop: string;
        number: number;
        scalarArray: number[];
    }
    relationships: {
        oneBar: ToOneRelationship<Bar>;
        barCollection: ToManyRelationship<Bar>;
    }
}

export default interface ResourceIndex extends Index {
    bars: Bar,
    foos: Foo,
}
