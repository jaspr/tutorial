<?php

/**
 * Created by tomas
 * at 04.09.2023
 */

declare(strict_types=1);

use DI\Container;
use JSONAPI\Expression\Dispatcher\ClosureResolver;
use JSONAPI\Mapper\Builder;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Driver\Driver;
use JSONAPI\Mapper\Encoding\Encoder;
use JSONAPI\Mapper\Encoding\EncoderFactory;
use JSONAPI\Mapper\IndexDocument;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Metadata\MetadataRepository;
use JSONAPI\Mapper\Middleware\PsrJsonApiMiddleware;
use JSONAPI\Mapper\ObjectCollection;
use JSONAPI\Mapper\OpenAPISpecificationBuilder;
use JSONAPI\Mapper\Request\Pagination\LimitOffsetResult;
use JSONAPI\Mapper\Request\Parser;
use JSONAPI\Mapper\TypeScriptInterfaceBuilder;
use JSONAPI\OAS\Contact;
use JSONAPI\OAS\Info;
use JSONAPI\OAS\OAuthFlow;
use JSONAPI\OAS\OAuthFlows;
use JSONAPI\OAS\SecurityScheme;
use JSONAPI\OAS\Type\In;
use JSONAPI\OAS\Type\SecuritySchemeScheme;
use JSONAPI\OAS\Type\SecuritySchemeType;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\SimpleCache\CacheInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Factory\AppFactory;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Routing\RouteContext;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;
use JSONAPI\Tutorial\Entity\Foo;

use function DI\autowire;
use function DI\factory;
use function DI\get;

require_once __DIR__ . '/../vendor/autoload.php';

/*
 * A little bit of DI setup
 */
$container = new Container();
$container->set('baseUrl', 'http://localhost');
$container->set(Driver::class, autowire(AnnotationDriver::class));
$container->set(CacheItemPoolInterface::class, autowire(ArrayAdapter::class));
$container->set(CacheInterface::class, autowire(Psr16Cache::class));
$container->set(
    MetadataRepository::class,
    factory(MetadataFactory::create(...))->parameter('paths', [__DIR__ . '/../src/Entity'])
);
$container->set(Encoder::class, factory(EncoderFactory::createRequestDependentEncoder(...)));
$container->set(Parser::class, autowire()->constructorParameter('baseUrl', get('baseUrl')));
$container->set(ResponseFactoryInterface::class, autowire(ResponseFactory::class));
$container->set(StreamFactoryInterface::class, autowire(StreamFactory::class));
$container->set(PsrJsonApiMiddleware::class, autowire());


/*
 * Some dummy data preparation
 */
/** @var ObjectCollection<Foo> $data */
$data = new ObjectCollection([], 50);
for ($i = 1; $i <= 50; $i++) {
    $entity = new Foo();
    $entity->id = (string)$i;
    $entity->prop = "value$i";
    $entity->number = $i;
    $data->add($entity);
}

/*
 * Here we create app from container
 */
$app = AppFactory::createFromContainer($container);

$app->options(
    '/{routes:.*}',
    fn(ServerRequestInterface $request, ResponseInterface $response) => $response->withStatus(204)
);
$app->get(
    "/",
    function (ServerRequestInterface $request, ResponseInterface $response) use ($container): ResponseInterface {
        $index = new IndexDocument($container->get(MetadataRepository::class), $container->get('baseUrl'));
        $response->getBody()->write(json_encode($index));
        return $response->withHeader('Content-Type', 'application/vnd.api+json');
    }
);
$app->get(
    '/ts',
    function (ServerRequestInterface $request, ResponseInterface $response) use ($container): ResponseInterface {
        $mr = $container->get(MetadataRepository::class);
        $builder = new TypeScriptInterfaceBuilder($mr);
        $builder->build();
        $sf = $container->get(StreamFactoryInterface::class);
        $body = $sf->createStream($builder->build());
        return $response
            ->withHeader('Content-Type', 'application/force-download')
            ->withHeader('Content-Type', 'application/octet-stream')
            ->withHeader('Content-Type', 'application/download')
            ->withHeader('Content-Description', 'File Transfer')
            ->withHeader('Content-Transfer-Encoding', 'binary')
            ->withHeader('Content-Disposition', 'attachment; filename="' . basename('index.ts') . '"')
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
            ->withHeader('Pragma', 'public')
            ->withBody($body);
    }
);
$app->get(
    "/oas",
    function (ServerRequestInterface $request, ResponseInterface $response) use ($container): ResponseInterface {
        $factory = new OpenAPISpecificationBuilder(
            $container->get(MetadataRepository::class),
            $container->get('baseUrl')
        );
        $info = new Info('API Schema', '3.0.1');
        $info->setContact((new Contact())->setEmail('tomas.benedikt@gmail.com'));
        $oas = $factory->create($info);
        $response->getBody()->write(json_encode($oas));
        return $response->withHeader('Content-Type', 'application/vnd.api+json');
    }
);
/*
 * Register collection route for resource Foo
 */
$app->get(
    '/foos',
    function (ServerRequestInterface $request, ResponseInterface $response) use ($data): ResponseInterface {
        /** @var Builder $builder */
        $builder = $request->getAttribute(PsrJsonApiMiddleware::BUILDER);

        $filter = $builder->getRequest()->getFilter();
        if ($expression = $filter->getCondition()) {
            $dispatcher = new ClosureResolver();
            $closure = $dispatcher->dispatch($expression);
            $data = $data->filter($closure);
        }

        $sort = $builder->getRequest()->getSort();
        if ($sort = $sort->getOrder()) {
            $data = $data->sort($sort);
        }

        /** @var LimitOffsetResult $pagination */
        $pagination = $builder->getRequest()->getPagination();
        $total = $data->count();
        $data = $data->slice($pagination->getOffset(), $pagination->getLimit());

        $doc = $builder->setData($data)->setTotal($total)->build();
        $doc->getMeta()->setProperty('total', $total);
        $response->getBody()->write(json_encode($doc));
        return $response;
    }
)->addMiddleware($container->get(PsrJsonApiMiddleware::class));
/*
 * Register route for single resource Foo
 */
$app->get(
    '/foos/{id}',
    function (ServerRequestInterface $request, ResponseInterface $response) use ($data): ResponseInterface {
        /** @var Builder $builder */
        $builder = $request->getAttribute(PsrJsonApiMiddleware::BUILDER);
        $id = $builder->getRequest()->getPath()->getId();
        $result = $data->filter(fn(Foo $item) => $item->id === $id);
        $entity = $result->values()[0];
        $doc = $builder->setData($entity)->build();
        $response->getBody()->write(json_encode($doc));
        return $response;
    }
)->addMiddleware($container->get(PsrJsonApiMiddleware::class));
/*
 * In case of bad call
 */
$app->map(
    ['GET', 'POST', 'DELETE', 'PATCH'],
    '/{routes:.+}',
    function (ServerRequestInterface $req, ResponseInterface $res) {
        throw new HttpNotFoundException($req);
    }
);
/*
 * CORS
 */
$app->addMiddleware(
    new class () implements MiddlewareInterface {
        public function process(
            ServerRequestInterface $request,
            RequestHandlerInterface $handler
        ): ResponseInterface {
            //'X-Requested-With, Content-Type, Accept, Origin, Authorization, Cache-Control'
            $response = $handler->handle($request);
            $response = $response->withHeader('Access-Control-Allow-Origin', '*');
            $response = $response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
            $response = $response->withHeader(
                'Access-Control-Allow-Headers',
                'X-Requested-With, Content-Type, Accept, Origin, Authorization, Cache-Control'
            );
            return $response;
        }
    }
);
/*
 * Just to see if something is not right
 */
$app->addErrorMiddleware(true, false, false);
/*
 * Let's roll!
 * To start a server just write to console
 * php -S localhost:80
 */
$app->run();
