<?php

/**
 * Created by tomas
 * at 04.09.2023
 */

declare(strict_types=1);

use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Mapper\Builder;
use JSONAPI\Mapper\Document\Document;
use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Encoding\EncoderFactory;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use JSONAPI\Mapper\Middleware\PsrJsonApiMiddleware;
use JSONAPI\Mapper\Request\Parser;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\ServerRequestFactory;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Response;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;
use JSONAPI\Tutorial\Entity\Foo;

require_once __DIR__ . '/../vendor/autoload.php';

$responseFactory = new ResponseFactory();
$streamFactory = new StreamFactory();
$baseUrl = 'http://localhost';
$cache = new Psr16Cache(new ArrayAdapter());
$driver = new AnnotationDriver();
$metadata = MetadataFactory::create(
    [__DIR__ . '/../src/Entity'],
    $cache,
    $driver
);
$parser = new Parser($baseUrl, $metadata);
$encoder = EncoderFactory::createRequestDependentEncoder($metadata);
$requestFactory = new ServerRequestFactory();
$url = 'http://localhost/foos/uuid';
$request = $requestFactory->createServerRequest(
    RequestMethodInterface::METHOD_GET,
    $url,
    ['Content-Type', Document::MEDIA_TYPE]
);

/*
 * Create middleware is simple, most dependencies will be provided from DI.
 * The middleware is PSR compliant, so should fit to many popular frameworks.
 */
$middleware = new PsrJsonApiMiddleware($responseFactory, $streamFactory, $parser, $encoder);

/*
 * This here is just helper for example. Handlers usually provide framework
 */
$handler = new class implements RequestHandlerInterface {
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var Builder $builder */
        $builder = $request->getAttribute(PsrJsonApiMiddleware::BUILDER);
        /*
         * You have access to encoder
         */
        $encoder = $builder->getEncoder();
        /*
         * And you have access to parsed request as well
         */
        $parsedRequest = $builder->getRequest();
        /*
         * Here will be parsed body as Document instance
         */
        $parsedRequest->getBody();
        /*
         * LimitOffset or PageSize strategy pagination, or your own
         */
        $parsedRequest->getPagination();
        $parsedRequest->getFieldset();
        $parsedRequest->getPath();
        $parsedRequest->getSort();
        $parsedRequest->getInclusion();
        $parsedRequest->getFilter();

        /*
         * Some data gathering
         */
        $data = new Foo();
        /*
         * Simple as that, we create response Document
         */
        $document = $builder->setData($data)->build();

        $factory = new StreamFactory();
        $stream = $factory->createStream(json_encode($document));

        return new Response(body: $stream);
    }
};
$response = $middleware->process($request, $handler);

echo "This is output document we send to client" . PHP_EOL;
echo "=========================================" . PHP_EOL;
echo json_encode(json_decode($response->getBody()->getContents(), true), JSON_PRETTY_PRINT);

return $middleware;
