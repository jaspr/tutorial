<?php

/**
 * Created by tomas
 * at 06.08.2023
 */

declare(strict_types=1);

use JSONAPI\Mapper\Driver\AnnotationDriver;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

require_once __DIR__ . '/../vendor/autoload.php';


/*
 * Creating MetadataRepository is crucial part of SDK
 */
/*
 * Cache serves as memory for Mapper, it saves your time. But for developing purpose use something like array for caching
 */
$cache = new Psr16Cache(new ArrayAdapter());
/*
 * Another part is driver. This argument varies if you are using attribute-driven approach, or schema-driven approach.
 * Attribute-driven uses PHP Attributes to describe your resource. Schema-driven uses interface ResourceSchema to receive
 * metadata about your resource
 */
$driver = new AnnotationDriver();
/*
 * Then we just create MetadataRepository with MetadataFactory
 */
$metadata = MetadataFactory::create(
    [__DIR__ . '/../src/Entity'],
    $cache,
    $driver
);
foreach ($metadata->getAll() as $metadata) {
    print_r($metadata);
}
/*
 * As you can see some metadata like type, or data type of properties was filled up automatically
 */
