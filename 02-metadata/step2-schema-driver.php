<?php
/**
 * Created by tomas
 * at 06.08.2023
 */

declare(strict_types=1);

use JSONAPI\Mapper\Driver\SchemaDriver;
use JSONAPI\Mapper\Metadata\MetadataFactory;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;

require_once __DIR__ . '/../vendor/autoload.php';

$cache = new Psr16Cache(new ArrayAdapter());
/*
 * Here we change our driver for SchemaDriver to see a difference
 */
$driver = new SchemaDriver();
$metadata = MetadataFactory::create(
    [__DIR__ . '/../src/Entity'],
    $cache,
    $driver
);
foreach ($metadata->getAll() as $metadata) {
    print_r($metadata);
}
/*
 * As you can see there is no difference in metadata.
 */
