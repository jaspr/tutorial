<?php
/**
 * Created by tomas
 * at 06.08.2023
 */

declare(strict_types=1);

namespace JSONAPI\Tutorial\Entity;

use JSONAPI\Mapper\Annotation\Attribute;
use JSONAPI\Mapper\Annotation\Id;
use JSONAPI\Mapper\Annotation\Relationship;
use JSONAPI\Mapper\Annotation\Resource;
use JSONAPI\Mapper\Schema\ResourceSchema;

#[Resource]
class Foo implements \JSONAPI\Mapper\Schema\Resource
{
    #[Id]
    public string $id = 'uuid';

    #[Attribute]
    public string $prop = 'data';

    #[Attribute]
    public int $number = 1;

    #[Attribute(of: 'int')]
    public array $scalarArray = [1, 2, 3];

    #[Relationship(Bar::class)]
    public Bar $oneBar;

    #[Relationship(Bar::class)]
    public array $barCollection = [];

    public function __construct()
    {
        $this->oneBar = new Bar();
        $this->barCollection = [new Bar(), new Bar()];
    }
    public static function getSchema(): ResourceSchema
    {
        return new ResourceSchema(
            __CLASS__,
            \JSONAPI\Mapper\Metadata\Id::createByProperty('id'),
            null, // <-- This means the drive gonna create type from class name
            [
                \JSONAPI\Mapper\Metadata\Attribute::createByProperty('foo'),
                \JSONAPI\Mapper\Metadata\Attribute::createByProperty('number'),
                \JSONAPI\Mapper\Metadata\Attribute::createByProperty('scalarArray', 'int')
            ],
            [
                \JSONAPI\Mapper\Metadata\Relationship::createByProperty('oneBar', Bar::class),
                \JSONAPI\Mapper\Metadata\Relationship::createByProperty('barCollection', Bar::class)
            ]
        );
    }
}
