<?php
/**
 * Created by tomas
 * at 06.08.2023
 */

declare(strict_types=1);

namespace JSONAPI\Tutorial\Entity;

use JSONAPI\Mapper\Annotation\Id;
use JSONAPI\Mapper\Annotation\Resource;
use JSONAPI\Mapper\Schema\ResourceSchema;

#[Resource]
class Bar implements \JSONAPI\Mapper\Schema\Resource
{
    #[Id]
    public string $id = 'uuid';

    public static function getSchema(): ResourceSchema
    {
        return new ResourceSchema(
            __CLASS__,
            \JSONAPI\Mapper\Metadata\Id::createByProperty('id')
        );
    }
}
