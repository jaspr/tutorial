<?php

/**
 * Created by tomas
 * at 05.08.2023
 */

declare(strict_types=1);

use JSONAPI\Mapper\Annotation\Attribute;
use JSONAPI\Mapper\Annotation\Id;
use JSONAPI\Mapper\Annotation\Relationship;
use JSONAPI\Mapper\Annotation\Resource;

/*
 * In this example we define relationship
 */

/*
 * This is our relationship class
 */

#[Resource]
class Bar
{
    #[Id]
    public string $id = 'uuid';
}

/*
 * Then we add property $relation holding instance of relationship. And $collection for related resource collection
 */

#[Resource]
class Foo
{
    #[Id]
    public string $id = 'uuid';

    #[Attribute]
    public string $prop = 'data';

    #[Attribute('someNumber')]
    public int $number = 1;

    #[Attribute(of: 'int')]
    public array $scalarArray = [1, 2, 3];

    #[Relationship(Bar::class)]
    public Bar $relation;

    #[Relationship(Bar::class)]
    public array $collection = [];
}
/*
 * As you cas see ToOne and ToMany relationship is described same way, Mapper recognize cardinality by data type.
 */
