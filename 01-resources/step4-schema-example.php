<?php

/**
 * Created by tomas
 * at 05.08.2023
 */

declare(strict_types=1);

use JSONAPI\Mapper\Metadata\Attribute;
use JSONAPI\Mapper\Metadata\Id;
use JSONAPI\Mapper\Metadata\Relationship;
use JSONAPI\Mapper\Schema\Resource;
use JSONAPI\Mapper\Schema\ResourceSchema;

class Bar
{
    public string $id = 'uuid';
}

/*
 * If you don't want to use PHP Attributes, you can choose schema driven approach
 */
class Foo implements Resource // <-- This interface mark class as resource
{
    public string $id = 'uuid';

    public string $prop = 'data';

    public int $number = 1;

    public array $scalarArray = [1, 2, 3];

    public Bar $relation;

    public array $collection = [];

    public static function getSchema(): ResourceSchema
    {
        return new ResourceSchema(
            __CLASS__,
            Id::createByProperty('id'),
            null, // <-- This means the drive gonna create type from class name
            [
                Attribute::createByProperty('prop'),
                Attribute::createByProperty('number'),
                Attribute::createByProperty('scalarArray', 'int')
            ],
            [
                Relationship::createByProperty('relation', Bar::class),
                Relationship::createByProperty('collection', Bar::class)
            ]
        );
    }
}
/*
 * As you can see, definition is very similar to attribute-driven approach.
 */
