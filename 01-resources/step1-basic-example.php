<?php

/**
 * Created by tomas
 * at 05.08.2023
 */

declare(strict_types=1);

use JSONAPI\Mapper\Annotation\Id;
use JSONAPI\Mapper\Annotation\Resource;

/*
 * First we define our resource by Attributes
 */

#[Resource] // <-- This is required
class Foo
{
    #[Id] // <-- This is required
    public string $id = 'uuid';
}

/*
 * This is minimal requirement for resource definition
 */
