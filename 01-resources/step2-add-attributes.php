<?php

/**
 * Created by tomas
 * at 05.08.2023
 */

declare(strict_types=1);

use JSONAPI\Mapper\Annotation\Attribute;
use JSONAPI\Mapper\Annotation\Id;
use JSONAPI\Mapper\Annotation\Resource;

/*
 * Here we add some attributes
 */

#[Resource]
class Foo
{
    #[Id]
    public string $id = 'uuid';

    /*
     * the minimal definition
     */
    #[Attribute]
    public string $prop = 'data';

    /*
     * You can rename attribute if you want
     */
    #[Attribute('someNumber')]
    public int $number = 1;

    /*
     * the array attribute has exception, you must define type of items, to provide type check in expression.
     * Even mixed is accepted
     */
    #[Attribute(of: 'int')]
    public array $scalarArray = [1, 2, 3];

}
